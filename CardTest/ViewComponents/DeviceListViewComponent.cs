﻿using CardTest.Data;
using CardTest.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace CardTest.ViewComponents
{
	public class DeviceListViewComponent : ViewComponent
	{
		public IViewComponentResult Invoke()
		{
			var model = new List<DeviceListItem>
			{
				new DeviceListItem
				{ 
					// boot up
					NodeID = 1,
					NMTState = NMTState.BootUp,
					LSSId = new UniqueLSSId
					{
						VendorID = 1,
						ProductID = 2,
						RevisionNumber = 1,
						SerialNumber = 123
					},
					TimeFromLastHeartbeat = TimeSpan.FromMilliseconds(10),
					VendorString = "Acme",
					ProductString = "testProduct",
					IsBootloader = true,
					EdsLoaded = true,
				},
				new DeviceListItem
				{
					// pre-op
					NodeID = 2,
					NMTState = NMTState.PreOperational,
					LSSId = new UniqueLSSId
					{
						VendorID = 1234,
						ProductID = 2535,
						RevisionNumber = 7,
						SerialNumber = 1
					},
					TimeFromLastHeartbeat = TimeSpan.FromMilliseconds(128),
					VendorString = "TestVendor",
					ProductString = "Product1",
					EdsLoaded = true,
				},
				new DeviceListItem
				{
					// op
					NodeID = 3,
					NMTState = NMTState.Operatuional,
					LSSId = new UniqueLSSId
					{
						VendorID = 3871,
						ProductID = 2001,
						RevisionNumber = 13,
						SerialNumber = 789
					},
					TimeFromLastHeartbeat = TimeSpan.FromSeconds(1),
					VendorString = "Рога и копыта",
					ProductString = "CAN Open Pressure sensor",
					EdsLoaded = true,
				},
				new DeviceListItem
				{
					// stopped
					NodeID = 4,
					NMTState = NMTState.Stopped,
					LSSId = new UniqueLSSId
					{
						VendorID = 3287,
						ProductID = 789,
						RevisionNumber = 4,
						SerialNumber = 1987
					},
					TimeFromLastHeartbeat = TimeSpan.FromMilliseconds(10),
					VendorString = "Unknown vendor",
					ProductString = "Unknown device"
				},
				new DeviceListItem
				{
					// unknown
					NodeID = 42,
					NMTState = NMTState.Unknown,
					LSSId = new UniqueLSSId(),
					TimeFromLastHeartbeat = TimeSpan.FromMinutes(1),
					VendorString = "0x12345678",
					ProductString = "0x9ABCDEE0"
				},
			};

			return View(model);
		}
	}
}