﻿using CardTest.Data;
using CardTest.Models.DeviceCardModels;
using Microsoft.AspNetCore.Mvc;
using System;

namespace CardTest.ViewComponents
{
	public class DeviceCardHeaderViewComponent : ViewComponent
	{
		public IViewComponentResult Invoke(string MarkClass, byte NodeID, NMTState NmtState, TimeSpan Heartbeat, bool AvatarReady)
		{
			var model = new DeviceCardHeaderModel
			{
				MarkClass = MarkClass,
				NodeID = NodeID,
				NmtStateString = NmtState.GetDescription(),
				AvatarIcon = AvatarReady ? "map-marker-alt" : "question"
			};

			if (Heartbeat < TimeSpan.FromSeconds(1))
			{
				model.HeartbeatToolTip = "Менее секунды назад";
				model.HeartBitIconColor = "success";
			}
			else if (Heartbeat < TimeSpan.FromSeconds(5))
			{
				model.HeartbeatToolTip = $"{Heartbeat:s\\.f} секунд назад";
				model.HeartBitIconColor = "warning";
			}
			else
			{
				model.HeartbeatToolTip = $"{Heartbeat} секунд назад";
				model.HeartBitIconColor = "danger";
			}

			return View(model);
		}
	}
}