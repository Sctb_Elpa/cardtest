﻿using CardTest.Models.DeviceCardModels;
using Microsoft.AspNetCore.Mvc;

namespace CardTest.ViewComponents
{
	public class DeviceCardBodyViewComponent : ViewComponent
	{
		public IViewComponentResult Invoke(string MarkClass, string Vendor, string Product,
			uint Revision, uint Serial, bool EdsReload)
		{
			return View(new DeviceCardBodyModel
			{
				MarkClass = MarkClass,
				Vendor = Vendor,
				Product = Product,
				Revision = Revision,
				Serial = Serial,
				EDSButtonTooltip = (EdsReload ? "Изменить" : "Загрузить") + " EDS"
			});
		}
	}
}