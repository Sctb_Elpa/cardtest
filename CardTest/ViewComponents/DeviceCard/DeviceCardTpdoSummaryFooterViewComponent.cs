﻿using CardTest.Models.DeviceCardModels;
using Microsoft.AspNetCore.Mvc;

namespace CardTest.ViewComponents
{
	public class DeviceCardTpdoSummaryFooterViewComponent : ViewComponent
	{
		public IViewComponentResult Invoke()
		{
			return View(new DeviceCardTpdoSummaryFooterModel());
		}
	}
}