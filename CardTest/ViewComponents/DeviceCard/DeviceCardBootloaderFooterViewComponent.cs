﻿using CardTest.Models.DeviceCardModels;
using Microsoft.AspNetCore.Mvc;

namespace CardTest.ViewComponents
{
	public class DeviceCardBootloaderFooterViewComponent : ViewComponent
	{
		public IViewComponentResult Invoke(byte NodeID, uint AppID)
		{
			return View(new DeviceCardBootloaderFooterModel
			{
				NodeID = NodeID,
				AppID = $"0x{AppID:X8}"
			});
		}
	}
}