﻿using CardTest.Models;
using Microsoft.AspNetCore.Mvc;

namespace CardTest.ViewComponents
{
	public class DeviceListItemViewComponent : ViewComponent
	{
		public IViewComponentResult Invoke(DeviceListItem item)
		{
			return View(item);
		}
	}
}
