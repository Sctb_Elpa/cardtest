﻿using CardTest.Data;
using System;
using System.Collections.Generic;

namespace CardTest.Models
{
	public class DeviceListItem
	{
		private Dictionary<NMTState, string> markClasses = new Dictionary<NMTState, string>
		{
			{ NMTState.BootUp, "info" },
			{ NMTState.PreOperational, "warning" },
			{ NMTState.Operatuional, "success" },
			{ NMTState.Stopped, "danger" },
			{ NMTState.Unknown, "secondary" },
		};

		public byte NodeID { get; set; }
		public NMTState NMTState { get; set; }

		public UniqueLSSId LSSId { get; set; }

		public TimeSpan TimeFromLastHeartbeat { get; set; }

		/*
		public DeviceState StateMonitor { get; set; }

		public int DeviceType { get; set; }

		public string VendorName { get; set; }
		public string DeviceName { get; set; }
		public bool IsEdsLoaded { get; set; }
		public int TPDOConfigured { get; set; }
		*/

		public string MarkClass => markClasses[NMTState];

		public string NMTStateString => NMTState.GetDescription();

		public string HeartBitIconColor
		{
			get
			{
				if (TimeFromLastHeartbeat < TimeSpan.FromSeconds(1))
				{
					return "success";
				}
				else if (TimeFromLastHeartbeat < TimeSpan.FromSeconds(5))
				{
					return "warning";
				}
				else
				{
					return "danger";
				}
			}
		}

		public string VendorString { get; set; }
		public string ProductString { get; set; }

		public bool IsBootloader { get; set; }

		public bool EdsLoaded { get; set; }

		public string HeartbeatText
		{
			get
			{
				if (TimeFromLastHeartbeat < TimeSpan.FromSeconds(1))
				{
					return "Менее секунды назад";
				}
				else if (TimeFromLastHeartbeat < TimeSpan.FromSeconds(5))
				{
					return $"{TimeFromLastHeartbeat:s\\.f} секунд назад";
				}
				else
				{
					return $"{TimeFromLastHeartbeat} секунд назад";
				}
			}
		}
	}
}