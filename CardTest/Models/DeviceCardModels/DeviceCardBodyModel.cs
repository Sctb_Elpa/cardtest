﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardTest.Models.DeviceCardModels
{
	public class DeviceCardBodyModel
	{
		public string MarkClass { get; internal set; }
		public string Vendor { get; internal set; }
		public string Product { get; internal set; }
		public uint Revision { get; internal set; }
		public uint Serial { get; internal set; }
		public string EDSButtonTooltip { get; internal set; }
	}
}
