﻿namespace CardTest.Models.DeviceCardModels
{
	public class DeviceCardBootloaderFooterModel
	{
		public byte NodeID { get; set; }
		public string AppID { get; set; }
	}
}