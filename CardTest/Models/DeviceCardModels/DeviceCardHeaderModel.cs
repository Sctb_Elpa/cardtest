﻿namespace CardTest.Models.DeviceCardModels
{
	public class DeviceCardHeaderModel
	{
		public string MarkClass { get; internal set; }
		public byte NodeID { get; internal set; }
		public string NmtStateString { get; internal set; }
		public string AvatarIcon { get; internal set; }
		public string HeartbeatToolTip { get; internal set; }
		public string HeartBitIconColor { get; internal set; }
	}
}
